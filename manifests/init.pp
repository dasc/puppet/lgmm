class lgmm (
    Array[String] $groups,
    Array[String] $ldap_urls,
    String $site,
    String $whitelist,
    String $blacklist,
    Optional[String] $filter = undef,
) {
    package{'ligo-grid-mapfile-manager':
        ensure => 'installed',
    }

    file { '/etc/lgmm/lgmm_config.py':
	ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('lgmm/lgmm_config.py.epp', {
	    groups => $groups,
	    site => $site,
	    whitelist => $whitelist,
	    blacklist => $blacklist,
	    ldap_urls => $ldap_urls,
	    filter => $filter,
	}),
	require => Package['ligo-grid-mapfile-manager'],
    }
    cron { "lgmm":
	command => "/usr/sbin/lgmm -f > /var/log/lgmm/lgmm_cron.log 2>&1",
	user => root,
	minute => 0,
    }
}
